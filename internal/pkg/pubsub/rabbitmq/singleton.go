package rabbitmq

import (
	"sync"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-amqp/pkg/amqp"
	"github.com/ThreeDotsLabs/watermill/message"
)

var (
	mqSubscriber      message.Subscriber
	mqPublisher       message.Publisher
	onceAMPQSubcribe  sync.Once
	onceAMPQPublisher sync.Once
)

func NewProviderSubscriberWithAMQP(loggerAdapter watermill.LoggerAdapter, amqpConfig amqp.Config) message.Subscriber {
	onceAMPQSubcribe.Do(func() {
		var err error

		mqSubscriber, err = amqp.NewSubscriber(
			amqpConfig,
			loggerAdapter,
		)
		if err != nil {
			panic(err)
		}

	})
	return mqSubscriber

}

func NewProviderPublihserWithAMQP(loggerAdapter watermill.LoggerAdapter, amqpConfig amqp.Config) message.Publisher {
	onceAMPQPublisher.Do(func() {
		var err error

		mqPublisher, err = amqp.NewPublisher(
			amqpConfig,
			loggerAdapter,
		)
		if err != nil {
			panic(err)
		}

	})
	return mqPublisher

}
