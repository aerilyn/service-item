package usecase

//go:generate mockgen -source=detail.go -destination=usecasemock/detail_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-item/internal/app/ent"
	"gitlab.com/aerilyn/service-library/errors"
)

type Detail interface {
	Detail(ctx context.Context, id string) (*ent.Item, errors.CodedError)
}
