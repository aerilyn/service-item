package usecase

//go:generate mockgen -source=list.go -destination=usecasemock/list_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-item/internal/app/ent"
	"gitlab.com/aerilyn/service-library/errors"
)

type ListCommand struct {
	Page  int64
	Limit int64
}

type ItemsDTO struct {
	Items []ent.Item `json:"items"`
}

type List interface {
	List(ctx context.Context, cmd ListCommand) (*ItemsDTO, errors.CodedError)
}
