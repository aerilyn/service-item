package usecase

//go:generate mockgen -source=create.go -destination=usecasemock/create_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type ItemCreateCommand struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	Price       int      `json:"price"`
	Description string   `json:"description"`
	Image       []string `json:"image"`
}

type ItemCreateDTO struct {
	ID string `json:"id"`
}

type Create interface {
	Create(ctx context.Context, cmd ItemCreateCommand) (*ItemCreateDTO, errors.CodedError)
}
