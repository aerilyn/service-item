package validatorimpl

import (
	"context"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/validation/ozzomapper"
)

type CreateValidatorOptions struct {
}

type CreateValidator struct {
	Options CreateValidatorOptions
}

func NewCreateValidator(opts CreateValidatorOptions) *CreateValidator {
	return &CreateValidator{
		Options: opts,
	}
}

func (s CreateValidator) Validate(ctx context.Context, cmd usecase.ItemCreateCommand) errors.CodedError {
	validationErr := validation.ValidateStruct(
		&cmd,
		validation.Field(&cmd.Name, validation.Required),
		validation.Field(&cmd.Price, validation.Required),
		validation.Field(&cmd.Description, validation.Required),
		validation.Field(&cmd.Image, validation.Required),
	)
	switch e := validationErr.(type) {
	case validation.InternalError:
		return errors.NewInternalSystemError().CopyWith(errors.Message(e.Error()))
	case validation.Errors:
		invalidParameterErr := errors.NewInvalidParameterError()
		return ozzomapper.WrapValidationError(invalidParameterErr.Code(), invalidParameterErr.MessageKey(), invalidParameterErr.Message(), e)
	}
	return nil
}
