package usecaseimpl

import (
	"context"
	"time"

	"gitlab.com/aerilyn/service-item/internal/app/ent"
	"gitlab.com/aerilyn/service-item/internal/app/item/repository"
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
)

type CreateOptions struct {
	Config              config.ConfigEnv
	ItemRepository      repository.ItemRepository
	ItemCreateValidator usecase.CreateValidator
}

type Create struct {
	options CreateOptions
}

func NewCreate(options CreateOptions) *Create {
	return &Create{
		options: options,
	}
}

func (s *Create) Create(ctx context.Context, cmd usecase.ItemCreateCommand) (*usecase.ItemCreateDTO, errors.CodedError) {
	err := s.options.ItemCreateValidator.Validate(ctx, cmd)
	if err != nil {
		return nil, err
	}
	ent := ent.Item{
		Name:        cmd.Name,
		Price:       cmd.Price,
		Description: cmd.Description,
		IsActive:    true,
		IsDelete:    false,
		Image:       cmd.Image,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}
	id, err := s.options.ItemRepository.Create(ctx, ent)
	if err != nil {
		return nil, err
	}

	dto := usecase.ItemCreateDTO{
		ID: *id,
	}
	return &dto, nil
}
