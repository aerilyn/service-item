package usecaseimpl

import (
	"context"

	"gitlab.com/aerilyn/service-item/internal/app/item/repository"
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase"
	"gitlab.com/aerilyn/service-item/internal/pkg/cache"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gopkg.in/mgo.v2/bson"
)

type ListOptions struct {
	Config         config.ConfigEnv
	ItemRepository repository.ItemRepository
	Cache          cache.Cache
}

type List struct {
	options ListOptions
}

func NewList(options ListOptions) *List {
	return &List{
		options: options,
	}
}

func (s *List) List(ctx context.Context, cmd usecase.ListCommand) (*usecase.ItemsDTO, errors.CodedError) {
	findOptions := repository.FindOptions{
		Limit:  cmd.Limit,
		Offset: (cmd.Page - 1) * cmd.Limit,
		Filter: bson.M{},
	}
	items, err := s.options.ItemRepository.FindAll(ctx, findOptions)
	if err != nil {
		return nil, err
	}

	itemsDTO := usecase.ItemsDTO{
		Items: items,
	}

	return &itemsDTO, nil
}
