package usecaseimpl

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/aerilyn/service-item/internal/app/item/repository/repositorymock"
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase"
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase/usecasemock"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
)

func TestItemCreate(t *testing.T) {
	ctx := context.Background()
	itemCreateCommand := usecase.ItemCreateCommand{
		Name:        "Item1",
		Description: "Deskripsi Item1",
		Price:       10000,
		Image:       []string{"base64:1", "base64:2"},
	}
	dto := usecase.ItemCreateDTO{
		ID: "123456789",
	}
	Convey("Testing Item Create", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		itemRepositoryMock := repositorymock.NewMockItemRepository(mockCtrl)
		itemCreateValidatorMock := usecasemock.NewMockCreateValidator(mockCtrl)
		createOptions := CreateOptions{
			Config:              configMock,
			ItemRepository:      itemRepositoryMock,
			ItemCreateValidator: itemCreateValidatorMock,
		}
		uc := NewCreate(createOptions)
		Convey("when validator return error should return error invalid", func() {
			itemCreateValidatorMock.EXPECT().Validate(ctx, itemCreateCommand).Return(errors.NewInvalidParameterError())

			res, errCode := uc.Create(ctx, itemCreateCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInvalidParameterError().Code())
		})
		Convey("when repository return error should return error internal service error", func() {
			itemCreateValidatorMock.EXPECT().Validate(ctx, itemCreateCommand).Return(nil)
			itemRepositoryMock.EXPECT().Create(ctx, gomock.Any()).Return(nil, errors.NewInternalSystemError())

			res, errCode := uc.Create(ctx, itemCreateCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInternalSystemError().Code())
		})
		Convey("when all process not return error  should return error nil", func() {
			itemCreateValidatorMock.EXPECT().Validate(ctx, itemCreateCommand).Return(nil)
			itemRepositoryMock.EXPECT().Create(ctx, gomock.Any()).Return(&dto.ID, nil)

			res, errCode := uc.Create(ctx, itemCreateCommand)
			So(errCode, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res, ShouldResemble, &dto)
		})
	})
}
