package usecaseimpl

import (
	"context"
	"encoding/json"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/aerilyn/service-item/internal/app/ent"
	"gitlab.com/aerilyn/service-item/internal/app/item/repository/repositorymock"
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase"
	cacheMock "gitlab.com/aerilyn/service-item/internal/pkg/cache/mock"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
)

func TestItemDetail(t *testing.T) {
	ctx := context.Background()
	itemID := "123456789"
	keyItem := usecase.CacheItem + itemID
	item := ent.Item{
		ID:          itemID,
		Name:        "Item1",
		Description: "Deskripsi Item1",
		Price:       10000,
		Image:       []string{"base64:1", "base64:2"},
	}
	byteJsonItem, _ := json.Marshal(item)
	stringJsonItem := string(byteJsonItem)
	Convey("Testing Item Detail", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		itemRepositoryMock := repositorymock.NewMockItemRepository(mockCtrl)
		cacheMock := cacheMock.NewMockCache(mockCtrl)
		detailOptions := DetailOptions{
			Config:         configMock,
			ItemRepository: itemRepositoryMock,
			Cache:          cacheMock,
		}
		uc := NewDetail(detailOptions)

		Convey("when get cache return error should return error internal server error", func() {
			cacheMock.EXPECT().Get(ctx, keyItem).Return(nil, errors.NewInternalSystemError())
			res, errCode := uc.Detail(ctx, itemID)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInternalSystemError().Code())
		})

		Convey("when get cache is nil but item repository return error not found entity should return error not found entity", func() {
			cacheMock.EXPECT().Get(ctx, keyItem).Return(nil, nil)
			itemRepositoryMock.EXPECT().Find(ctx, itemID).Return(nil, errors.NewEntityNotFound())
			res, errCode := uc.Detail(ctx, itemID)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewEntityNotFound().Code())
		})

		Convey("when get cache is nil but item repository return error internal system should return error  error internal system", func() {
			cacheMock.EXPECT().Get(ctx, keyItem).Return(nil, nil)
			itemRepositoryMock.EXPECT().Find(ctx, itemID).Return(nil, errors.NewInternalSystemError())
			res, errCode := uc.Detail(ctx, itemID)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInternalSystemError().Code())
		})

		Convey("when get cache return value but set cache error should return error internal system", func() {
			cacheMock.EXPECT().Get(ctx, keyItem).Return(nil, nil)
			itemRepositoryMock.EXPECT().Find(ctx, itemID).Return(&item, nil)
			cacheMock.EXPECT().Set(ctx, keyItem, byteJsonItem, gomock.Any()).Return(errors.NewInternalSystemError())
			res, errCode := uc.Detail(ctx, itemID)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInternalSystemError().Code())
		})

		Convey("when get cache is nil and all process not return error  should return error nil", func() {
			cacheMock.EXPECT().Get(ctx, keyItem).Return(nil, nil)
			itemRepositoryMock.EXPECT().Find(ctx, itemID).Return(&item, nil)
			cacheMock.EXPECT().Set(ctx, keyItem, byteJsonItem, gomock.Any()).Return(nil)
			res, errCode := uc.Detail(ctx, itemID)
			So(errCode, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res, ShouldResemble, &item)
		})

		Convey("when cache has item string json and all process not return error should return error nil", func() {
			cacheMock.EXPECT().Get(ctx, keyItem).Return(&stringJsonItem, nil)
			res, errCode := uc.Detail(ctx, itemID)
			So(errCode, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res, ShouldResemble, &item)
		})
	})
}
