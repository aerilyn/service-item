package usecaseimpl

import (
	"context"
	"encoding/json"
	"time"

	"gitlab.com/aerilyn/service-item/internal/app/ent"
	"gitlab.com/aerilyn/service-item/internal/app/item/repository"
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase"
	"gitlab.com/aerilyn/service-item/internal/pkg/cache"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
)

type DetailOptions struct {
	Config         config.ConfigEnv
	ItemRepository repository.ItemRepository
	Cache          cache.Cache
}

type Detail struct {
	options DetailOptions
}

func NewDetail(options DetailOptions) *Detail {
	return &Detail{
		options: options,
	}
}

func (s *Detail) Detail(ctx context.Context, id string) (*ent.Item, errors.CodedError) {
	item := &ent.Item{}
	keyItem := usecase.CacheItem + id
	cache, err := s.options.Cache.Get(ctx, keyItem)
	if err != nil {
		return nil, err
	}

	if cache == nil {
		item, err = s.options.ItemRepository.Find(ctx, id)
		if err != nil {
			return nil, err
		}
		duration := time.Hour * time.Duration(1)
		stringJson, _ := json.Marshal(item)
		err = s.options.Cache.Set(ctx, keyItem, stringJson, duration)
		if err != nil {
			return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
		}
	}

	if cache != nil {
		errUnmarshal := json.Unmarshal([]byte(*cache), item)
		if errUnmarshal != nil {
			return nil, errors.NewInternalSystemError().CopyWith(errors.Message(errUnmarshal.Error()))
		}
	}

	return item, nil
}
