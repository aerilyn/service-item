package usecaseimpl

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/aerilyn/service-item/internal/app/ent"
	"gitlab.com/aerilyn/service-item/internal/app/item/repository"
	"gitlab.com/aerilyn/service-item/internal/app/item/repository/repositorymock"
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase"
	cacheMock "gitlab.com/aerilyn/service-item/internal/pkg/cache/mock"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
	"gopkg.in/mgo.v2/bson"
)

func TestItemList(t *testing.T) {
	ctx := context.Background()
	listCommand := usecase.ListCommand{
		Page:  10,
		Limit: 10,
	}
	item := ent.Item{
		Name:        "Item1",
		Description: "Deskripsi Item1",
		Price:       10000,
		Image:       []string{"base64:1", "base64:2"},
	}

	items := []ent.Item{
		item,
		item,
		item,
	}

	itemsDTO := usecase.ItemsDTO{
		Items: items,
	}
	findOptions := repository.FindOptions{
		Limit:  listCommand.Limit,
		Offset: (listCommand.Page - 1) * listCommand.Limit,
		Filter: bson.M{},
	}
	Convey("Testing Item List", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		itemRepositoryMock := repositorymock.NewMockItemRepository(mockCtrl)
		cacheMock := cacheMock.NewMockCache(mockCtrl)
		listOptions := ListOptions{
			Config:         configMock,
			ItemRepository: itemRepositoryMock,
			Cache:          cacheMock,
		}
		uc := NewList(listOptions)

		Convey("when get item from FindAll return error should return internal system error", func() {
			emptyItems := []ent.Item{}
			itemRepositoryMock.EXPECT().FindAll(ctx, findOptions).Return(emptyItems, errors.NewInternalSystemError())
			res, errCode := uc.List(ctx, listCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInternalSystemError().Code())
		})

		Convey("when all process not return error  should return error nil", func() {
			itemRepositoryMock.EXPECT().FindAll(ctx, findOptions).Return(items, nil)
			res, errCode := uc.List(ctx, listCommand)
			So(errCode, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res, ShouldResemble, &itemsDTO)
		})
	})
}
