package item

import (
	"gitlab.com/aerilyn/service-item/internal/app/item/delivery/web"
	"gitlab.com/aerilyn/service-item/internal/app/item/repository"
	"gitlab.com/aerilyn/service-item/internal/app/item/repository/repositorymongo"
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase"
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase/usecaseimpl"
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase/validatorimpl"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	repositoryModuleSet,
	validatorModuleSet,
	usecaseModuleSet,
	deliveryModuleSet,
)

var repositoryModuleSet = wire.NewSet(
	wire.Struct(new(repositorymongo.MongoItemRepositoryOptions), "*"),
	repositorymongo.NewMongoItemRepository,
	wire.Bind(new(repository.ItemRepository), new(*repositorymongo.MongoItemRepository)),
)

var deliveryModuleSet = wire.NewSet(
	wire.Struct(new(web.ItemRegistryOptions), "*"),
	web.NewItemHandlerRegistry,
)

var validatorModuleSet = wire.NewSet(
	wire.Struct(new(validatorimpl.CreateValidatorOptions), "*"),
	validatorimpl.NewCreateValidator,
	wire.Bind(new(usecase.CreateValidator), new(*validatorimpl.CreateValidator)),
)

var usecaseModuleSet = wire.NewSet(
	wire.Struct(new(usecaseimpl.CreateOptions), "*"),
	usecaseimpl.NewCreate,
	wire.Bind(new(usecase.Create), new(*usecaseimpl.Create)),

	wire.Struct(new(usecaseimpl.DetailOptions), "*"),
	usecaseimpl.NewDetail,
	wire.Bind(new(usecase.Detail), new(*usecaseimpl.Detail)),

	wire.Struct(new(usecaseimpl.ListOptions), "*"),
	usecaseimpl.NewList,
	wire.Bind(new(usecase.List), new(*usecaseimpl.List)),
)
