// Code generated by MockGen. DO NOT EDIT.
// Source: item.go

// Package repositorymock is a generated GoMock package.
package repositorymock

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	ent "gitlab.com/aerilyn/service-item/internal/app/ent"
	repository "gitlab.com/aerilyn/service-item/internal/app/item/repository"
	errors "gitlab.com/aerilyn/service-library/errors"
)

// MockItemRepository is a mock of ItemRepository interface.
type MockItemRepository struct {
	ctrl     *gomock.Controller
	recorder *MockItemRepositoryMockRecorder
}

// MockItemRepositoryMockRecorder is the mock recorder for MockItemRepository.
type MockItemRepositoryMockRecorder struct {
	mock *MockItemRepository
}

// NewMockItemRepository creates a new mock instance.
func NewMockItemRepository(ctrl *gomock.Controller) *MockItemRepository {
	mock := &MockItemRepository{ctrl: ctrl}
	mock.recorder = &MockItemRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockItemRepository) EXPECT() *MockItemRepositoryMockRecorder {
	return m.recorder
}

// Create mocks base method.
func (m *MockItemRepository) Create(ctx context.Context, cmd ent.Item) (*string, errors.CodedError) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", ctx, cmd)
	ret0, _ := ret[0].(*string)
	ret1, _ := ret[1].(errors.CodedError)
	return ret0, ret1
}

// Create indicates an expected call of Create.
func (mr *MockItemRepositoryMockRecorder) Create(ctx, cmd interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockItemRepository)(nil).Create), ctx, cmd)
}

// Find mocks base method.
func (m *MockItemRepository) Find(ctx context.Context, id string) (*ent.Item, errors.CodedError) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Find", ctx, id)
	ret0, _ := ret[0].(*ent.Item)
	ret1, _ := ret[1].(errors.CodedError)
	return ret0, ret1
}

// Find indicates an expected call of Find.
func (mr *MockItemRepositoryMockRecorder) Find(ctx, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Find", reflect.TypeOf((*MockItemRepository)(nil).Find), ctx, id)
}

// FindAll mocks base method.
func (m *MockItemRepository) FindAll(ctx context.Context, opt repository.FindOptions) ([]ent.Item, errors.CodedError) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FindAll", ctx, opt)
	ret0, _ := ret[0].([]ent.Item)
	ret1, _ := ret[1].(errors.CodedError)
	return ret0, ret1
}

// FindAll indicates an expected call of FindAll.
func (mr *MockItemRepositoryMockRecorder) FindAll(ctx, opt interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FindAll", reflect.TypeOf((*MockItemRepository)(nil).FindAll), ctx, opt)
}

// FindByListID mocks base method.
func (m *MockItemRepository) FindByListID(ctx context.Context, listID []string) ([]ent.Item, errors.CodedError) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FindByListID", ctx, listID)
	ret0, _ := ret[0].([]ent.Item)
	ret1, _ := ret[1].(errors.CodedError)
	return ret0, ret1
}

// FindByListID indicates an expected call of FindByListID.
func (mr *MockItemRepositoryMockRecorder) FindByListID(ctx, listID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FindByListID", reflect.TypeOf((*MockItemRepository)(nil).FindByListID), ctx, listID)
}
