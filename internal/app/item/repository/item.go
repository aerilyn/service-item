package repository

//go:generate mockgen -source=item.go -destination=repositorymock/item_mock.go -package=repositorymock

import (
	"context"

	"gitlab.com/aerilyn/service-item/internal/app/ent"
	"gitlab.com/aerilyn/service-library/errors"
	"gopkg.in/mgo.v2/bson"
)

type FindOptions struct {
	Limit  int64
	Offset int64
	Filter bson.M
}

type ItemRepository interface {
	Create(ctx context.Context, cmd ent.Item) (*string, errors.CodedError)
	Find(ctx context.Context, id string) (*ent.Item, errors.CodedError)
	FindAll(ctx context.Context, opt FindOptions) ([]ent.Item, errors.CodedError)
	FindByListID(ctx context.Context, listID []string) ([]ent.Item, errors.CodedError)
}
