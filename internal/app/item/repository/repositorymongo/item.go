package repositorymongo

import (
	"context"
	"fmt"

	"gitlab.com/aerilyn/service-item/internal/app/ent"
	"gitlab.com/aerilyn/service-item/internal/app/item/repository"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

type MongoItemRepositoryOptions struct {
	Db *mongo.Database
}

type MongoItemRepository struct {
	options MongoItemRepositoryOptions
}

func NewMongoItemRepository(options MongoItemRepositoryOptions) *MongoItemRepository {
	return &MongoItemRepository{options: options}
}

func (m *MongoItemRepository) collection() *mongo.Collection {
	return m.options.Db.Collection(CollectionItem)
}

func (m *MongoItemRepository) Create(ctx context.Context, cmd ent.Item) (*string, errors.CodedError) {
	result, err := m.collection().InsertOne(ctx, cmd)
	if err != nil {
		return nil, errors.NewInternalSystemError()
	}
	id := fmt.Sprintf("%v", result.InsertedID.(primitive.ObjectID).Hex())
	return &id, nil
}

func (m *MongoItemRepository) FindAll(ctx context.Context, opt repository.FindOptions) ([]ent.Item, errors.CodedError) {
	opts := options.Find().
		SetSkip(opt.Offset).
		SetLimit(opt.Limit).
		SetSort(bson.M{"created_at": -1})
	items := []ent.Item{}
	filter := bson.M{"isDelete": false}
	cursor, err := m.collection().Find(ctx, filter, opts)
	if err == mongo.ErrNoDocuments {
		return nil, errors.NewEntityNotFound()
	}

	if err != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}

	if errResult := cursor.All(ctx, &items); errResult != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(errResult.Error()))
	}

	return items, nil
}

func (m *MongoItemRepository) Find(ctx context.Context, id string) (*ent.Item, errors.CodedError) {
	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}
	filter := bson.M{"_id": objectId}
	item := ent.Item{}
	findErr := m.collection().FindOne(ctx, filter).Decode(&item)
	if findErr == mongo.ErrNoDocuments {
		return nil, errors.NewEntityNotFound()
	}
	if findErr != nil {
		return nil, errors.NewInternalSystemError()
	}
	return &item, nil
}

func (m *MongoItemRepository) FindByListID(ctx context.Context, listID []string) ([]ent.Item, errors.CodedError) {
	var listObjectId []primitive.ObjectID
	items := []ent.Item{}
	for _, id := range listID {
		objectId, err := primitive.ObjectIDFromHex(id)
		if err != nil {
			log.WithCTX(ctx).Info(fmt.Sprintf("error when convert objectID => %s", id))
			return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
		}
		listObjectId = append(listObjectId, objectId)
	}
	filter := bson.M{"_id": bson.M{"$in": listObjectId}}
	cursor, err := m.collection().Find(ctx, filter)
	if err == mongo.ErrNoDocuments {
		return nil, errors.NewEntityNotFound()
	}

	if err != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}

	if errResult := cursor.All(ctx, &items); errResult != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(errResult.Error()))
	}
	return items, nil
}
