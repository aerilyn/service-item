package web

import (
	"gitlab.com/aerilyn/service-item/internal/app/item/usecase"

	"github.com/go-chi/chi"
)

type ItemRegistryOptions struct {
	ItemCreate usecase.Create
	ItemDetail usecase.Detail
	ItemList   usecase.List
}

type ItemHandlerRegistry struct {
	options ItemRegistryOptions
}

func NewItemHandlerRegistry(options ItemRegistryOptions) *ItemHandlerRegistry {
	return &ItemHandlerRegistry{
		options: options,
	}
}

func (h *ItemHandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Route("/admin", func(r chi.Router) {
		r.Route("/item", func(r chi.Router) {
			r.Group(func(r chi.Router) {
				r.Post("/", ItemCreateWebHandler(h.options.ItemCreate))
			})
		})
	})
	r.Route("/item", func(r chi.Router) {
		r.Group(func(r chi.Router) {
			r.Get("/{id}/", ItemDetailWebHandler(h.options.ItemDetail))
		})
	})
	r.Route("/items", func(r chi.Router) {
		r.Group(func(r chi.Router) {
			r.Get("/", ItemListWebHandler(h.options.ItemList))
		})
	})
	return nil
}
