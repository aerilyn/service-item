package web

import (
	"net/http"
	"strconv"

	"gitlab.com/aerilyn/service-item/internal/app/item/usecase"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/aerilyn/service-library/response"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

const (
	defaultPage  = 1
	defaultLimit = 25
)

func ItemCreateWebHandler(itemCreate usecase.Create) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		cmd := usecase.ItemCreateCommand{}
		if decodeErr := render.DecodeJSON(r.Body, &cmd); decodeErr != nil {
			err := errors.NewInternalSystemError().CopyWith(errors.Message(decodeErr.Error()))
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		result, err := itemCreate.Create(ctx, cmd)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusCreated, nil})
	}
}

func ItemDetailWebHandler(itemDetail usecase.Detail) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		id := chi.URLParam(r, "id")
		result, err := itemDetail.Detail(ctx, id)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusOK, nil})
	}
}

func ItemListWebHandler(itemList usecase.List) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		page, errResult := strconv.ParseInt(r.URL.Query().Get("page"), 10, 64)
		if errResult != nil {
			page = defaultPage
		}

		if page == 0 {
			page = 1
		}

		limit, errResult := strconv.ParseInt(r.URL.Query().Get("limit"), 10, 64)
		if errResult != nil {
			limit = defaultLimit
		}

		cmd := usecase.ListCommand{
			Page:  page,
			Limit: limit,
		}
		result, err := itemList.List(ctx, cmd)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusOK, nil})
	}
}
