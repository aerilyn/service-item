package wishlist

import (
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/delivery/web"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/repository"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/repository/repositorymongo"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase/usecaseimpl"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase/validatorimpl"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	repositoryModuleSet,
	validatorModuleSet,
	usecaseModuleSet,
	deliveryModuleSet,
)

var repositoryModuleSet = wire.NewSet(
	wire.Struct(new(repositorymongo.MongoWishlistRepositoryOptions), "*"),
	repositorymongo.NewMongoWishlistRepository,
	wire.Bind(new(repository.WishlistRepository), new(*repositorymongo.MongoWishlistRepository)),
)

var deliveryModuleSet = wire.NewSet(
	wire.Struct(new(web.RegistryOptions), "*"),
	web.NewHandlerRegistry,
)

var validatorModuleSet = wire.NewSet(
	wire.Struct(new(validatorimpl.AddValidatorOptions), "*"),
	validatorimpl.NewAddValidator,
	wire.Bind(new(usecase.AddValidator), new(*validatorimpl.AddValidator)),
	wire.Struct(new(validatorimpl.RemoveValidatorOptions), "*"),
	validatorimpl.NewRemoveValidator,
	wire.Bind(new(usecase.RemoveValidator), new(*validatorimpl.RemoveValidator)),
)

var usecaseModuleSet = wire.NewSet(
	wire.Struct(new(usecaseimpl.ListOptions), "*"),
	usecaseimpl.NewList,
	wire.Bind(new(usecase.WishlistList), new(*usecaseimpl.List)),
	wire.Struct(new(usecaseimpl.AddOptions), "*"),
	usecaseimpl.NewAdd,
	wire.Bind(new(usecase.WishlistAdd), new(*usecaseimpl.Add)),
	wire.Struct(new(usecaseimpl.RemoveOptions), "*"),
	usecaseimpl.NewRemove,
	wire.Bind(new(usecase.WishlistRemove), new(*usecaseimpl.Remove)),
)
