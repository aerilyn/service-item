package slice

func Paginate(x []string, page int, limit int) []string {
	skip := (page - 1) * limit
	end := skip + limit
	if skip > len(x) {
		skip = len(x)
	}

	if end > len(x) {
		end = len(x)
	}

	return x[skip:end]
}
