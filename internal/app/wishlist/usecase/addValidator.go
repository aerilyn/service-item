package usecase

//go:generate mockgen -source=addValidator.go -destination=usecasemock/addValidator_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type AddValidator interface {
	Validate(ctx context.Context, cmd AddCommand) errors.CodedError
}
