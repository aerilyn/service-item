package usecase

//go:generate mockgen -source=list.go -destination=usecasemock/list_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-item/internal/app/ent"
	"gitlab.com/aerilyn/service-library/errors"
)

type ListCommand struct {
	UserID string
	Page   int64
	Limit  int64
}

type ListDTO struct {
	Wishlist []ent.Item `json:"wishlist"`
}

type WishlistList interface {
	List(ctx context.Context, cmd ListCommand) (*ListDTO, errors.CodedError)
}
