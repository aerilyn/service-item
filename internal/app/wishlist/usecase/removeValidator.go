package usecase

//go:generate mockgen -source=removeValidator.go -destination=usecasemock/removeValidator_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type RemoveValidator interface {
	Validate(ctx context.Context, cmd RemoveCommand) errors.CodedError
}
