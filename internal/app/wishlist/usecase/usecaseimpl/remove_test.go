package usecaseimpl

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	repositoryWishlistMock "gitlab.com/aerilyn/service-item/internal/app/wishlist/repository/repositorymock"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase/usecasemock"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
)

func TestWishlistRemove(t *testing.T) {
	ctx := context.Background()
	removeCommand := usecase.RemoveCommand{
		ItemID: "I1234567890",
		UserID: "1234567890",
	}

	dtoSuccess := usecase.RemoveDTO{
		Status: true,
	}

	Convey("Testing Wishlist Remove", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		repositoryWishlistMock := repositoryWishlistMock.NewMockWishlistRepository(mockCtrl)
		removeValidatorMock := usecasemock.NewMockRemoveValidator(mockCtrl)
		removeOptions := RemoveOptions{
			Config:             configMock,
			WishlistRepository: repositoryWishlistMock,
			RemoveValidator:    removeValidatorMock,
		}
		uc := NewRemove(removeOptions)
		Convey(`when validator return error invalid parameter should return error invalid parameter and res nil`, func() {
			removeValidatorMock.EXPECT().Validate(ctx, removeCommand).Return(errors.NewInvalidParameterError())
			res, errCode := uc.Remove(ctx, removeCommand)
			So(res, ShouldBeNil)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInvalidParameterError().Code())
		})
		Convey(`when find error internal system should return error internal system and res nil`, func() {
			removeValidatorMock.EXPECT().Validate(ctx, removeCommand).Return(nil)
			repositoryWishlistMock.EXPECT().RemoveItemID(ctx, removeCommand.UserID, removeCommand.ItemID).Return(errors.NewInternalSystemError())
			res, errCode := uc.Remove(ctx, removeCommand)
			So(res, ShouldBeNil)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey(`when all process not return error should return error nil
		        and res`, func() {
			removeValidatorMock.EXPECT().Validate(ctx, removeCommand).Return(nil)
			repositoryWishlistMock.EXPECT().RemoveItemID(ctx, removeCommand.UserID, removeCommand.ItemID).Return(nil)
			res, errCode := uc.Remove(ctx, removeCommand)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dtoSuccess)
		})
	})
}
