package usecaseimpl

import (
	"context"

	"gitlab.com/aerilyn/service-item/internal/app/wishlist/repository"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
)

type RemoveOptions struct {
	Config             config.ConfigEnv
	WishlistRepository repository.WishlistRepository
	RemoveValidator    usecase.RemoveValidator
}

type Remove struct {
	options RemoveOptions
}

func NewRemove(options RemoveOptions) *Remove {
	return &Remove{
		options: options,
	}
}

func (s *Remove) Remove(ctx context.Context, cmd usecase.RemoveCommand) (*usecase.RemoveDTO, errors.CodedError) {
	dto := usecase.RemoveDTO{
		Status: false,
	}
	err := s.options.RemoveValidator.Validate(ctx, cmd)
	if err != nil {
		log.WithCTX(ctx).Error(err)
		return nil, err
	}

	err = s.options.WishlistRepository.RemoveItemID(ctx, cmd.UserID, cmd.ItemID)
	if err != nil && err.Code() == errors.NewEntityNotFound().Code() {
		log.WithCTX(ctx).Error(err)
		return &dto, err
	}
	if err != nil {
		log.WithCTX(ctx).Error(err)
		return nil, err
	}
	dto.Status = true
	return &dto, nil
}
