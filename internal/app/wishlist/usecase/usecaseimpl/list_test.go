package usecaseimpl

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/aerilyn/service-item/internal/app/ent"
	repositoryItemMock "gitlab.com/aerilyn/service-item/internal/app/item/repository/repositorymock"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/helper/slice"
	repositoryWishlistMock "gitlab.com/aerilyn/service-item/internal/app/wishlist/repository/repositorymock"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
)

func TestWishlistList(t *testing.T) {
	ctx := context.Background()
	listCommand := usecase.ListCommand{
		Page:   1,
		Limit:  10,
		UserID: "1234567890",
	}

	item := ent.Item{
		Name:        "Item1",
		Description: "Deskripsi Item1",
		Price:       10000,
		Image:       []string{"base64:1", "base64:2"},
	}

	wishlist := ent.Wishlist{
		ID:     "1234567890",
		UserID: "1234567890",
		ItemID: []string{"1234567891", "1234567892", "1234567893", "1234567894"},
	}

	item1 := item
	item1.ID = wishlist.ItemID[0]
	item2 := item
	item2.ID = wishlist.ItemID[1]
	item3 := item
	item3.ID = wishlist.ItemID[2]
	item4 := item
	item4.ID = wishlist.ItemID[3]

	items1 := []ent.Item{
		item1,
	}

	items2 := []ent.Item{
		item2,
	}

	items3 := []ent.Item{}

	items4 := []ent.Item{
		item1,
		item2,
		item3,
		item4,
	}

	dto1 := usecase.ListDTO{
		Wishlist: items1,
	}

	dto2 := usecase.ListDTO{
		Wishlist: items2,
	}

	dto3 := usecase.ListDTO{
		Wishlist: items3,
	}

	dto4 := usecase.ListDTO{
		Wishlist: items4,
	}

	Convey("Testing Wishlist List", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		repositoryWishlistMock := repositoryWishlistMock.NewMockWishlistRepository(mockCtrl)
		repositoryItemMock := repositoryItemMock.NewMockItemRepository(mockCtrl)
		listOptions := ListOptions{
			Config:             configMock,
			ItemRepository:     repositoryItemMock,
			WishlistRepository: repositoryWishlistMock,
		}
		uc := NewList(listOptions)
		Convey(`when find return error internal system should return error internal system`, func() {
			repositoryWishlistMock.EXPECT().Find(ctx, listCommand.UserID).Return(nil, errors.NewInternalSystemError())
			res, errCode := uc.List(ctx, listCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInternalSystemError().Code())
		})
		Convey(`when find return error internal system should return error nil and result return wishlist with empty array`, func() {
			repositoryWishlistMock.EXPECT().Find(ctx, listCommand.UserID).Return(nil, errors.NewEntityNotFound())
			res, errCode := uc.List(ctx, listCommand)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dto3)
		})
		Convey(`when FindByListID return error internal system should return error internal system`, func() {
			listItemID := slice.Paginate(wishlist.ItemID, int(listCommand.Page), int(listCommand.Limit))
			repositoryWishlistMock.EXPECT().Find(ctx, listCommand.UserID).Return(&wishlist, nil)
			repositoryItemMock.EXPECT().FindByListID(ctx, listItemID).Return(items3, errors.NewInternalSystemError())
			res, errCode := uc.List(ctx, listCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInternalSystemError().Code())
		})
		Convey(`when all process not return error should return error nil
		        where cmd limit = 1 and page = 1`, func() {
			listCommand1Data := listCommand
			listCommand1Data.Limit = 1
			listCommand1Data.Page = 1
			listItemID := slice.Paginate(wishlist.ItemID, int(listCommand1Data.Page), int(listCommand1Data.Limit))
			repositoryWishlistMock.EXPECT().Find(ctx, listCommand1Data.UserID).Return(&wishlist, nil)
			repositoryItemMock.EXPECT().FindByListID(ctx, listItemID).Return(items1, nil)
			res, errCode := uc.List(ctx, listCommand1Data)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dto1)
		})
		Convey(`when all process not return error should return error nil
		        where cmd limit = 1 and page = 2`, func() {
			listCommand1Data := listCommand
			listCommand1Data.Limit = 1
			listCommand1Data.Page = 2
			listItemID := slice.Paginate(wishlist.ItemID, int(listCommand1Data.Page), int(listCommand1Data.Limit))
			repositoryWishlistMock.EXPECT().Find(ctx, listCommand1Data.UserID).Return(&wishlist, nil)
			repositoryItemMock.EXPECT().FindByListID(ctx, listItemID).Return(items2, nil)
			res, errCode := uc.List(ctx, listCommand1Data)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dto2)
		})
		Convey(`when all process not return error should return error nil
		        where cmd limit = 2 and page = 3 result is empty array string`, func() {
			listCommand1Data := listCommand
			listCommand1Data.Limit = 2
			listCommand1Data.Page = 3
			repositoryWishlistMock.EXPECT().Find(ctx, listCommand1Data.UserID).Return(&wishlist, nil)
			res, errCode := uc.List(ctx, listCommand1Data)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dto3)
		})
		Convey(`when all process not return error should return error nil
		        where cmd limit = 10 and page = 1 result is empty array string`, func() {
			listItemID := slice.Paginate(wishlist.ItemID, int(listCommand.Page), int(listCommand.Limit))
			repositoryWishlistMock.EXPECT().Find(ctx, listCommand.UserID).Return(&wishlist, nil)
			repositoryItemMock.EXPECT().FindByListID(ctx, listItemID).Return(items4, nil)
			res, errCode := uc.List(ctx, listCommand)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dto4)
		})
		Convey(`when all process not return error should return error nil
		        where cmd limit = 10 and page = 2 result is empty array string`, func() {
			listCommand1Data := listCommand
			listCommand1Data.Limit = 10
			listCommand1Data.Page = 1
			listItemID := slice.Paginate(wishlist.ItemID, int(listCommand1Data.Page), int(listCommand1Data.Limit))
			repositoryWishlistMock.EXPECT().Find(ctx, listCommand1Data.UserID).Return(&wishlist, nil)
			repositoryItemMock.EXPECT().FindByListID(ctx, listItemID).Return(items3, nil)
			res, errCode := uc.List(ctx, listCommand1Data)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dto3)
		})
	})
}
