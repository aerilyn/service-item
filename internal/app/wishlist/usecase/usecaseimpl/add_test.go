package usecaseimpl

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/aerilyn/service-item/internal/app/ent"
	repositoryItemMock "gitlab.com/aerilyn/service-item/internal/app/item/repository/repositorymock"
	repositoryWishlistMock "gitlab.com/aerilyn/service-item/internal/app/wishlist/repository/repositorymock"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase/usecasemock"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
)

func TestWishlistAdd(t *testing.T) {
	ctx := context.Background()
	addCommand := usecase.AddCommand{
		ItemID: "I1234567890",
		UserID: "1234567890",
	}

	item := ent.Item{
		ID:          "I1234567890",
		Name:        "item test 1",
		Price:       10000,
		Description: "description item test 1",
	}

	wishlist := ent.Wishlist{
		UserID: addCommand.UserID,
		ItemID: []string{addCommand.ItemID},
	}

	dtoFailed := usecase.AddDTO{
		Status: false,
	}

	dtoSuccess := usecase.AddDTO{
		Status: true,
	}

	Convey("Testing Wishlist Add", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		repositoryWishlistMock := repositoryWishlistMock.NewMockWishlistRepository(mockCtrl)
		repositoryItemMock := repositoryItemMock.NewMockItemRepository(mockCtrl)
		addValidatorMock := usecasemock.NewMockAddValidator(mockCtrl)
		addOptions := AddOptions{
			Config:             configMock,
			ItemRepository:     repositoryItemMock,
			WishlistRepository: repositoryWishlistMock,
			AddValidator:       addValidatorMock,
		}
		uc := NewAdd(addOptions)
		Convey(`when validator return error should return error invalid parameter `, func() {
			addValidatorMock.EXPECT().Validate(ctx, addCommand).Return(errors.NewInvalidParameterError())
			res, errCode := uc.Add(ctx, addCommand)
			So(res, ShouldBeNil)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInvalidParameterError().Code())
		})
		Convey(`when find item not found should return error entity not found and res status failed`, func() {
			addValidatorMock.EXPECT().Validate(ctx, addCommand).Return(nil)
			repositoryItemMock.EXPECT().Find(ctx, addCommand.ItemID).Return(nil, errors.NewEntityNotFound())
			res, errCode := uc.Add(ctx, addCommand)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewEntityNotFound().Code())
			So(res, ShouldResemble, &dtoFailed)
		})
		Convey(`when find error internal system should return error internal system and res nil`, func() {
			addValidatorMock.EXPECT().Validate(ctx, addCommand).Return(nil)
			repositoryItemMock.EXPECT().Find(ctx, addCommand.ItemID).Return(nil, errors.NewInternalSystemError())
			res, errCode := uc.Add(ctx, addCommand)
			So(res, ShouldBeNil)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey(`when RemoveItemID error internal system should return error internal system and res nil`, func() {
			addValidatorMock.EXPECT().Validate(ctx, addCommand).Return(nil)
			repositoryItemMock.EXPECT().Find(ctx, addCommand.ItemID).Return(&item, nil)
			repositoryWishlistMock.EXPECT().RemoveItemID(ctx, addCommand.UserID, item.ID).Return(errors.NewInternalSystemError())
			res, errCode := uc.Add(ctx, addCommand)
			So(res, ShouldBeNil)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey(`when AddItemID error internal system should return error internal system and res nil`, func() {
			addValidatorMock.EXPECT().Validate(ctx, addCommand).Return(nil)
			repositoryItemMock.EXPECT().Find(ctx, addCommand.ItemID).Return(&item, nil)
			repositoryWishlistMock.EXPECT().RemoveItemID(ctx, addCommand.UserID, item.ID).Return(nil)
			repositoryWishlistMock.EXPECT().AddItemID(ctx, addCommand.UserID, item.ID).Return(errors.NewInternalSystemError())
			res, errCode := uc.Add(ctx, addCommand)
			So(res, ShouldBeNil)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey(`when RemoveItemID return error entity not found and no other error should return error nil
		        and res`, func() {
			addValidatorMock.EXPECT().Validate(ctx, addCommand).Return(nil)
			repositoryItemMock.EXPECT().Find(ctx, addCommand.ItemID).Return(&item, nil)
			repositoryWishlistMock.EXPECT().RemoveItemID(ctx, addCommand.UserID, item.ID).Return(errors.NewEntityNotFound())
			repositoryWishlistMock.EXPECT().AddItemID(ctx, addCommand.UserID, item.ID).Return(errors.NewEntityNotFound())
			repositoryWishlistMock.EXPECT().Create(ctx, gomock.Any()).Return(&wishlist, nil)
			res, errCode := uc.Add(ctx, addCommand)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dtoSuccess)
		})
		Convey(`when all process not return error and wishlist not exists should return error nil
		        and res`, func() {
			addValidatorMock.EXPECT().Validate(ctx, addCommand).Return(nil)
			repositoryItemMock.EXPECT().Find(ctx, addCommand.ItemID).Return(&item, nil)
			repositoryWishlistMock.EXPECT().RemoveItemID(ctx, addCommand.UserID, item.ID).Return(nil)
			repositoryWishlistMock.EXPECT().AddItemID(ctx, addCommand.UserID, item.ID).Return(errors.NewEntityNotFound())
			repositoryWishlistMock.EXPECT().Create(ctx, gomock.Any()).Return(&wishlist, nil)
			res, errCode := uc.Add(ctx, addCommand)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dtoSuccess)
		})
		Convey(`when all process not return error and wishlist exists should return error nil
		        and res`, func() {
			addValidatorMock.EXPECT().Validate(ctx, addCommand).Return(nil)
			repositoryItemMock.EXPECT().Find(ctx, addCommand.ItemID).Return(&item, nil)
			repositoryWishlistMock.EXPECT().RemoveItemID(ctx, addCommand.UserID, item.ID).Return(nil)
			repositoryWishlistMock.EXPECT().AddItemID(ctx, addCommand.UserID, item.ID).Return(nil)
			res, errCode := uc.Add(ctx, addCommand)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dtoSuccess)
		})
	})
}
