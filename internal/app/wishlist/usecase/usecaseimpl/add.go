package usecaseimpl

import (
	"context"
	"time"

	"gitlab.com/aerilyn/service-item/internal/app/ent"
	repositoryItem "gitlab.com/aerilyn/service-item/internal/app/item/repository"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/repository"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
)

type AddOptions struct {
	Config             config.ConfigEnv
	ItemRepository     repositoryItem.ItemRepository
	WishlistRepository repository.WishlistRepository
	AddValidator       usecase.AddValidator
}

type Add struct {
	options AddOptions
}

func NewAdd(options AddOptions) *Add {
	return &Add{
		options: options,
	}
}

func (s *Add) Add(ctx context.Context, cmd usecase.AddCommand) (*usecase.AddDTO, errors.CodedError) {
	dto := usecase.AddDTO{
		Status: false,
	}

	err := s.options.AddValidator.Validate(ctx, cmd)
	if err != nil {
		log.WithCTX(ctx).Info("error when validate")
		return nil, err
	}

	item, err := s.options.ItemRepository.Find(ctx, cmd.ItemID)
	if err != nil && err.Code() == errors.NewEntityNotFound().Code() {
		return &dto, err
	}
	if err != nil {
		log.WithCTX(ctx).Info("error when item find")
		return nil, err
	}

	err = s.options.WishlistRepository.RemoveItemID(ctx, cmd.UserID, item.ID)
	if err != nil && err.Code() != errors.NewEntityNotFound().Code() {
		log.WithCTX(ctx).Info("error when RemoveItemID")
		return nil, err
	}

	err = s.options.WishlistRepository.AddItemID(ctx, cmd.UserID, item.ID)
	if err != nil && err.Code() == errors.NewEntityNotFound().Code() {
		wishlist := ent.Wishlist{
			UserID:    cmd.UserID,
			ItemID:    []string{cmd.ItemID},
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		}
		_, err = s.options.WishlistRepository.Create(ctx, wishlist)
		if err != nil {
			log.WithCTX(ctx).Info("error when create wishlist")
			return nil, err
		}
	}
	if err != nil {
		log.WithCTX(ctx).Info("error when additemid")
		return nil, err
	}
	dto.Status = true
	return &dto, nil
}
