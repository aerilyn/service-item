package usecaseimpl

import (
	"context"

	"gitlab.com/aerilyn/service-item/internal/app/ent"
	repositoryItem "gitlab.com/aerilyn/service-item/internal/app/item/repository"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/helper/slice"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/repository"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
)

type ListOptions struct {
	Config             config.ConfigEnv
	ItemRepository     repositoryItem.ItemRepository
	WishlistRepository repository.WishlistRepository
}

type List struct {
	options ListOptions
}

func NewList(options ListOptions) *List {
	return &List{
		options: options,
	}
}

func (s *List) List(ctx context.Context, cmd usecase.ListCommand) (
	*usecase.ListDTO, errors.CodedError) {
	items := []ent.Item{}
	dto := usecase.ListDTO{
		Wishlist: items,
	}
	wishlist, err := s.options.WishlistRepository.Find(ctx, cmd.UserID)
	if err != nil && err.Code() == errors.NewEntityNotFound().Code() {
		return &dto, nil
	}

	if err != nil {
		log.WithCTX(ctx).Info("error when find wishlist")
		return nil, errors.NewInternalSystemError()
	}

	wishlistPaginate := slice.Paginate(
		wishlist.ItemID,
		int(cmd.Page),
		int(cmd.Limit),
	)
	if len(wishlistPaginate) == 0 {
		return &dto, nil
	}
	items, err = s.options.ItemRepository.FindByListID(
		ctx,
		wishlistPaginate,
	)
	if err != nil {
		log.WithCTX(ctx).Info("error when find item by list ID")
		return nil, errors.NewInternalSystemError()
	}

	dto.Wishlist = items
	return &dto, nil
}
