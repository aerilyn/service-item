package usecase

//go:generate mockgen -source=add.go -destination=usecasemock/add_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type AddCommand struct {
	ItemID string `json:"item_id"`
	UserID string `json:"user_id"`
}

type AddDTO struct {
	Status bool `json:"status"`
}

type WishlistAdd interface {
	Add(ctx context.Context, cmd AddCommand) (*AddDTO, errors.CodedError)
}
