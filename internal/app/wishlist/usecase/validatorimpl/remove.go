package validatorimpl

import (
	"context"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/validation/ozzomapper"
)

type RemoveValidatorOptions struct {
}

type RemoveValidator struct {
	Options RemoveValidatorOptions
}

func NewRemoveValidator(opts RemoveValidatorOptions) *RemoveValidator {
	return &RemoveValidator{
		Options: opts,
	}
}

func (s RemoveValidator) Validate(ctx context.Context, cmd usecase.RemoveCommand) errors.CodedError {
	validationErr := validation.ValidateStruct(
		&cmd,
		validation.Field(&cmd.ItemID, validation.Required),
	)
	switch e := validationErr.(type) {
	case validation.InternalError:
		return errors.NewInternalSystemError().CopyWith(errors.Message(e.Error()))
	case validation.Errors:
		invalidParameterErr := errors.NewInvalidParameterError()
		return ozzomapper.WrapValidationError(invalidParameterErr.Code(), invalidParameterErr.MessageKey(), invalidParameterErr.Message(), e)
	}
	return nil
}
