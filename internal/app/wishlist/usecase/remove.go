package usecase

//go:generate mockgen -source=remove.go -destination=usecasemock/remove_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type RemoveCommand struct {
	ItemID string `json:"item_id"`
	UserID string `json:"user_id"`
}

type RemoveDTO struct {
	Status bool `json:"status"`
}

type WishlistRemove interface {
	Remove(ctx context.Context, cmd RemoveCommand) (*RemoveDTO, errors.CodedError)
}
