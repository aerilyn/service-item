package repository

//go:generate mockgen -source=wishlist.go -destination=repositorymock/wishlist_mock.go -package=repositorymock

import (
	"context"

	"gitlab.com/aerilyn/service-item/internal/app/ent"
	"gitlab.com/aerilyn/service-library/errors"
	"gopkg.in/mgo.v2/bson"
)

type FindOptions struct {
	Limit  int64
	Offset int64
	Filter bson.M
}

type WishlistRepository interface {
	Create(ctx context.Context, wishlist ent.Wishlist) (*ent.Wishlist, errors.CodedError)
	Find(ctx context.Context, userID string) (*ent.Wishlist, errors.CodedError)
	AddItemID(ctx context.Context, userID, ItemID string) errors.CodedError
	RemoveItemID(ctx context.Context, userID, ItemID string) errors.CodedError
}
