package repositorymongo

import (
	"context"
	"fmt"

	"gitlab.com/aerilyn/service-item/internal/app/ent"
	"gitlab.com/aerilyn/service-library/errors"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

type MongoWishlistRepositoryOptions struct {
	Db *mongo.Database
}

type MongoWishlistRepository struct {
	options MongoWishlistRepositoryOptions
}

func NewMongoWishlistRepository(options MongoWishlistRepositoryOptions) *MongoWishlistRepository {
	return &MongoWishlistRepository{options: options}
}

func (m *MongoWishlistRepository) collection() *mongo.Collection {
	return m.options.Db.Collection(CollectionWishlist)
}

func (m *MongoWishlistRepository) Find(ctx context.Context, userID string) (*ent.Wishlist, errors.CodedError) {
	filter := bson.M{"user_id": userID}
	wishlist := ent.Wishlist{}
	findErr := m.collection().FindOne(ctx, filter).Decode(&wishlist)
	if findErr == mongo.ErrNoDocuments {
		return nil, errors.NewEntityNotFound()
	}
	if findErr != nil {
		return nil, errors.NewInternalSystemError()
	}
	return &wishlist, nil
}

func (m *MongoWishlistRepository) Create(ctx context.Context, wishlist ent.Wishlist) (*ent.Wishlist, errors.CodedError) {
	result, err := m.collection().InsertOne(ctx, wishlist)
	if err != nil {
		return nil, errors.NewInternalSystemError()
	}
	wishlist.ID = fmt.Sprintf("%v", result.InsertedID.(primitive.ObjectID).Hex())
	return &wishlist, nil
}

func (m *MongoWishlistRepository) AddItemID(ctx context.Context, userID, ItemID string) errors.CodedError {
	filter := bson.M{"user_id": bson.M{"$eq": userID}}
	update := bson.M{"$push": bson.M{"item_id": ItemID}}
	result, err := m.collection().UpdateOne(ctx, filter, update)

	if err != nil {
		return errors.NewInternalSystemError()
	}
	if result.ModifiedCount != 1 {
		return errors.NewEntityNotFound()
	}
	return nil
}

func (m *MongoWishlistRepository) RemoveItemID(ctx context.Context, userID, ItemID string) errors.CodedError {
	filter := bson.M{"user_id": bson.M{"$eq": userID}}
	update := bson.M{"$pull": bson.M{"item_id": ItemID}}
	result, err := m.collection().UpdateOne(ctx, filter, update)

	if err != nil {
		return errors.NewInternalSystemError()
	}
	if result.ModifiedCount != 1 {
		return errors.NewEntityNotFound()
	}
	return nil
}
