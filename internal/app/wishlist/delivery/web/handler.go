package web

import (
	"net/http"
	"strconv"

	"github.com/go-chi/render"
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/aerilyn/service-library/middleware"
	"gitlab.com/aerilyn/service-library/response"
)

const (
	defaultPage  = 1
	defaultLimit = 25
)

func ListWebHandler(uc usecase.WishlistList) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		userID := ctx.Value(middleware.UserIDKey).(string)
		page, errResult := strconv.ParseInt(r.URL.Query().Get("page"), 10, 64)
		if errResult != nil {
			page = defaultPage
		}

		if page == 0 {
			page = 1
		}

		limit, errResult := strconv.ParseInt(r.URL.Query().Get("limit"), 10, 64)
		if errResult != nil {
			limit = defaultLimit
		}

		cmd := usecase.ListCommand{
			UserID: userID,
			Page:   page,
			Limit:  limit,
		}
		result, err := uc.List(ctx, cmd)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusOK, nil})
	}
}

func AddWebHandler(uc usecase.WishlistAdd) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		cmd := usecase.AddCommand{}
		if decodeErr := render.DecodeJSON(r.Body, &cmd); decodeErr != nil {
			log.WithCTX(ctx).Info("error when decode payload")
			err := errors.NewInternalSystemError().CopyWith(errors.Message(decodeErr.Error()))
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}

		cmd.UserID = ctx.Value(middleware.UserIDKey).(string)
		result, err := uc.Add(ctx, cmd)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, result, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusCreated, nil})
	}
}

func RemoveWebHandler(uc usecase.WishlistRemove) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		cmd := usecase.RemoveCommand{}
		if decodeErr := render.DecodeJSON(r.Body, &cmd); decodeErr != nil {
			log.WithCTX(ctx).Info("error when decode payload")
			err := errors.NewInternalSystemError().CopyWith(errors.Message(decodeErr.Error()))
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}

		cmd.UserID = ctx.Value(middleware.UserIDKey).(string)
		result, err := uc.Remove(ctx, cmd)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, result, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusAccepted, nil})
	}
}
