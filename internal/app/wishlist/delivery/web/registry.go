package web

import (
	"gitlab.com/aerilyn/service-item/internal/app/wishlist/usecase"

	"github.com/go-chi/chi"
)

type RegistryOptions struct {
	WishlistList   usecase.WishlistList
	WishlistAdd    usecase.WishlistAdd
	WishlistRemove usecase.WishlistRemove
}

type HandlerRegistry struct {
	options RegistryOptions
}

func NewHandlerRegistry(options RegistryOptions) *HandlerRegistry {
	return &HandlerRegistry{
		options: options,
	}
}

func (h *HandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Route("/wishlist", func(r chi.Router) {
		r.Get("/", ListWebHandler(h.options.WishlistList))
		r.Post("/", AddWebHandler(h.options.WishlistAdd))
		r.Delete("/", RemoveWebHandler(h.options.WishlistRemove))
	})
	return nil
}
