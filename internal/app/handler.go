package app

import (
	healthcheckweb "gitlab.com/aerilyn/service-item/internal/app/healthcheck/delivery/web"
	item "gitlab.com/aerilyn/service-item/internal/app/item/delivery/web"
	wishlist "gitlab.com/aerilyn/service-item/internal/app/wishlist/delivery/web"
)

// jhipster-needle-import-handler

type RequiredHandlers struct {
	ItemHTTPHandlerRegistry        *item.ItemHandlerRegistry
	WishListHTTPHandlerRegistry    *wishlist.HandlerRegistry
	HealthCheckHTTPHandlerRegistry *healthcheckweb.HealthCheckHandlerRegistry
}
