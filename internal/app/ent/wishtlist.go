package ent

import "time"

//for saving transaction ecommerce payment
type Wishlist struct {
	ID        string    `json:"id" bson:"_id,omitempty"`
	UserID    string    `json:"user_id" bson:"user_id"`
	ItemID    []string  `json:"item_id" bson:"item_id"`
	CreatedAt time.Time `json:"-" bson:"created_at"`
	UpdatedAt time.Time `json:"-" bson:"updated_at"`
}
