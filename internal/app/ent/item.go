package ent

import "time"

//for saving transaction ecommerce payment
type Item struct {
	ID          string    `json:"id" bson:"_id,omitempty"`
	Name        string    `json:"name" bson:"name"`
	Price       int       `json:"price" bson:"price"`
	Description string    `json:"description" bson:"description"`
	IsActive    bool      `json:"isActive" bson:"isActive"`
	IsDelete    bool      `json:"isDelete" bson:"isDelete"`
	Image       []string  `json:"image" bson:"image"`
	CreatedAt   time.Time `json:"-" bson:"created_at"`
	UpdatedAt   time.Time `json:"-" bson:"updated_at"`
}
